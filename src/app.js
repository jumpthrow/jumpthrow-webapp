import React,{useState,useEffect,useRef} from 'react'
import {app} from './app.css'
import {makeStyles,Button,List,ListItem,ListItemText,styled,Paper,Grid,Box} from '@material-ui/core'
import {sizing} from '@material-ui/system'
import { mergeClasses } from '@material-ui/styles'

const useStyles = makeStyles({
    root: {
      width: '100%',
      maxWidth: 360,
    },
    button:{
        background: '#61dafb',
        "&:hover":{
          backgroundColor:"#618cf9"
        },
        border: 0,
        borderRadius: 3,
        color: 'black',
        height: "100%",
        width: "100%",
        maxWidth:"300px",
        padding: "5%",
    },
    list:{
        width: '100%',
        overflow: "auto",
        backgroundColor: '#21242a',
        height: "100%",
        maxHeight: "80vh"
      },
    listItem:{
        
    },
    listItemSelected:{
      backgroundColor: null,
      color: "#ffffff",
      "&.Mui-selected":
      {backgroundColor:"#61dafb",
       color: "#000000",
      "&:hover":
      {backgroundColor:"#61dafb",color: "#000000",},},
      "&:hover":
      {backgroundColor:"#61dafb",color: "#000000",
    },
      
    
    },
    listText:{
      color: "#ffffff"
    },
    paper:{
      width:"100%",
      maxHeight:"100%",
      maxWidth: 400,
      color: '#21242a',
      padding: "10",
    },
    infopaper:{
      width: "100%",
      height: "100%",

    }


  })


  function getMaps(){

    return ["onemap"]
  }
  function getConfigs(){
    return ["1v1","5v5 Competitive","Nades","Deathmatch"]
  }



  function Selectlist(props){
    const [selectedIndex,setIndex]=useState(0);
    const classes=useStyles()
    const items=props.items
    const renderItems=items.map((item,index)=> 
    <ListItem className={classes.listItemSelected}
    button
    selected={selectedIndex === index}
    onClick={(event) =>handleItemClick(event, index)}>
    <ListItemText primary={item}/>
    </ListItem>)
    function handleItemClick(e,index){
      setIndex(index);
      props.updateFunction(index);
    }


    return (
      <Paper className= {classes.paper}>
      <div>
      
  <List className={classes.list}>
    {renderItems}
  </List>
</div>
</Paper>
    )
  }



function NewApp(props){

  const [maps,setMaps]=useState([""])
  const [configs,setConfigs]=useState([""])
  const currentMap=useRef(maps[0])
  const currentConfig=useRef(configs[0])

  
  function updateMap(index){
    currentMap.current=maps[index]
    console.log(currentMap.current)
  }
  function updateConfig(index){
    currentConfig.current=configs[index]
    console.log(currentConfig.current)
  }

  useEffect(()=>{
    fetch("http://localhost:5000/clientdata").then(response => response.json()).then(data=>{
      setMaps(data.Maps); 
      setConfigs(data.Configs);})
  },[])

  function createServer(){
    const requestOptions={
      method: "POST",
      mode:"no-cors",
      headers:{"Content-Type":"application/json",
    "Access-Control-Allow-Origin":"*"},
      body: JSON.stringify({Map:currentMap.current,
      Config:currentConfig.current,
      AdvancedOptions:[]
    })
    };
    fetch("http://localhost:5000/create",requestOptions).then(response=>response.json()).then()
  }

  const classes=useStyles()
  return (

    <Grid container spacing={3} direction="row" justify="center" height="95%" width="95%" alignItems="center" style={{padding: "4%"} }>
    <Grid item xs={4} height="100%">
    
    <Selectlist items={maps} updateFunction={updateMap}></Selectlist>
    </Grid>
    <Grid item xs={4} >
    <Selectlist items={configs} updateFunction={updateConfig}></Selectlist>
    </Grid>
    <Grid item xs={4} >

    <Grid container direction="column" justify="space-evenly" alignItems="center">
    <Grid item xs={4}>
    <Button variant="contained" className={classes.button} onClick={createServer}>start</Button>
    </Grid>

    <Grid item xs={4}>
    <Button variant="contained" className={classes.button}>placeholder</Button>
    </Grid>

    <Grid item xs={4}>
    <Button variant="contained" className={classes.button}>Advanced</Button>
    </Grid>
    </Grid>
    </Grid>
    </Grid>
  )
}








export default NewApp